# Restful Api Node.js

This is an example of making restful api with node js used for learning or as a master of initial creation of restful api with node.js

## Node packages
initializing the npm, install the following packages

```bash
npm i express express-validator mysql2 jsonwebtoken bcryptjs
```

## Mysql Database

```sql
CREATE TABLE users (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  email varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  password varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY email (email)
 )
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
